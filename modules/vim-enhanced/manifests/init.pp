class vim-enhanced {
  package {"vim-enhanced":
    ensure => installed,
  }
}
