class apache (
    $webhost = hiera("redis_host"),
		$webhost_info = hiera_hash($::redis_host)
		)
{
  package {'mod_ssl':
    ensure => installed,
  }

  package {'httpd':
    ensure => installed,
    require => Package['mod_ssl']
  }

  file {'/var/www':
    ensure => directory,
  }

  file {'/var/www/html':
    ensure => directory,
    require => File['/var/www'],
  }

  file {'/var/www/html/index.html':
    ensure => file,
    source => 'puppet:///modules/apache/index.html',
    require => File['/var/www/html'],
  }

  file {'/etc/httpd/conf/httpd.conf':
    ensure => file,
    # source => 'puppet:///modules/apache/httpd.conf',
		content => template('apache/httpd.conf.erb'),
    require => Package['httpd'],
  }

  file {'/etc/httpd/conf/mime.types':
    ensure => file,
    source => 'puppet:///modules/apache/mime.types',
    require => Package['httpd'],
  }

  service {'httpd':
    ensure => running,
		enable => true,
    require => [Package['httpd'], File['/etc/httpd/conf/mime.types']]
  }
}
