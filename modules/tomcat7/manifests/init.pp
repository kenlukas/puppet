class tomcat7 (
	$app_info = hiera('redis_host'),
  $apphost_info = hiera_hash($::redis_host)
	)

{
  exec { 'download java':
    command => "/usr/local/bin/aws s3 cp s3://teradevops/software-repo/jdk-7u80-linux-x64.rpm /tmp/jdk-7u80-linux-x64.rpm",
  }
#   package {'tomcat7':
#     ensure => installed,
#     source => '/tmp/jdk-7u80-linux-x64.rpm'
#   }
#
#   file {'/etc/profile.d/java.sh':
#     ensure => file,
#     content => "
# export CATALINA_HOME=/usr/share/tomcat
# ",
#     require => Package['tomcat'],
#   }
#
#   file {'/etc/tomcat/server.xml':
#     ensure => file,
#     content => template('tomcat/server.xml.erb'),
#     require => Package['tomcat'],
#   }
#
#   file {'/usr/share/tomcat/webapps/trm/WEB-INF/classes/jdbc.properties':
#     ensure => file,
#     content => template('tomcat/jdbc.properties.erb'),
#     require => File['/etc/tomcat/server.xml'],
#   }
#
#   file {'/usr/share/tomcat/webapps/trm/WEB-INF/classes/trm.properties':
#     ensure => file,
#     content => template('tomcat/trm.properties.erb'),
#     require => File['/usr/share/tomcat/webapps/trm/WEB-INF/classes/jdbc.properties'],
#   }
#
#   service {'tomcat':
#     ensure => running,
#     enable => true,
#     require => File['/etc/profile.d/java.sh'],
#   }

}
