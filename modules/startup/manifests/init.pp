class startup { 

	file {'/etc/selinux/config':
		ensure => 'present',
		source => 'puppet:///modules/startup/config',
		owner => 'root',
		group => 'root',
		mode => '0644',
	}
	
	file { '/etc/hiera.yaml':
		ensure => 'present',
		source => 'puppet:///modules/startup/hiera.yaml',
	}		
}