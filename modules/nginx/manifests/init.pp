class nginx {

# baseurl needs to be dynamic after prototype
  yumrepo { "redhat-rhui":
    ensure => present,
    baseurl => 'http://nginx.org/packages/rhel/7/x86_64/', 
    enabled => 1,
    gpgcheck => 0,
    descr => "nginx repo",
  }

  package {"nginx":
    ensure => installed,
    require => Yumrepo['redhat-rhui'],
  }


  file {"/var/www":
    ensure => directory,
    require => Package['nginx'],
  }

  file {"/var/www/index.html":
    ensure => present,
    source => 'puppet:///modules/nginx/index.html',
    require => [ Package['nginx'],File['/var/www'] ],
  }

  file {"/etc/nginx":
    ensure => directory,
  }

  file {"/etc/nginx/nginx.conf":
    ensure => present,
    source => 'puppet:///modules/nginx/nginx.conf',
    require => [ Package['nginx'], File["/etc/nginx"] ],
  }

  service {"nginx":
    ensure => running,
    require => [ Package['nginx'], File['/etc/nginx/nginx.conf']],
  }

}
