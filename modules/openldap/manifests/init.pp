class openldap {
  package {'openldap':
    ensure => installed,
  }

  package {'openldap-devel':
    ensure => installed,
    require => Package['openldap'],
  }

  package {'openldap-servers':
    ensure => installed,
    require => Package[['openldap'],['openldap-devel']]
  }

  package {'sssd':
    ensure => installed,
    require => Package[['openldap'], ['openldap-devel'], ['openldap-servers']],
  }

  package {'perl-LDAP.noarch':
    ensure => installed,
    require => Package[['openldap'], ['openldap-devel'], ['openldap-servers'], ['sssd']],
  }
  file{'/var/lib/ldap':
    ensure => directory,
    owner => ldap,
    group => ldap,
    require => Package['perl-LDAP.noarch'],
  }

  file {'/var/lib/ldap/DB_CONFIG':
    ensure => present,
    source => 'puppet:///modules/openldap/db_config',
    owner  => ldap,
    group =>  ldap,
    require => File['/var/lib/ldap'],
  }

  file {'/etc/openldap':
    ensure => directory,
    require => File['/var/lib/ldap'],
  }

  file {'/etc/openldap/slapd.conf':
    ensure => present,
    source => 'puppet:///modules/openldap/slapd.conf',
    require => File['/var/lib/ldap'],
  }
}
