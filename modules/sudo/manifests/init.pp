class sudo {
  
  package {'sudo':
    ensure => installed,
  }

 file {'/etc/sudoers':
    source => 'puppet:///modules/sudo/sudoers',
    ensure => file,
    owner => 'root',
    group => 'root',
    mode => '0440',
  }
}
