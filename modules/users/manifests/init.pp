class users {
	user { "ken":
		ensure			=> present,
		uid					=> '501',
		shell				=> '/bin/bash',
		home				=> '/home/ken',
		managehome	=> true,
    groups => ["wheel"],
	}

	group { "ken":
    gid					=> '501',
	}

	file { "/home/ken/.ssh":
		ensure			=> directory,
		owner				=> 'ken',
		group				=> 'ken',
		mode				=> '0700',
	}

  file { "/home/ken/.ssh/id_rsa.pub":
    source      => 'puppet:///modules/users/ken/id_rsa.pub',
    ensure      => file,
    owner       => 'ken',
    group       => 'ken',
    mode        => '0400'
  }

  file { "/home/ken/.ssh/authorized_keys":
    source      => 'puppet:///modules/users/ken/authorized_keys',
    ensure      => file,
    owner       => 'ken',
    group       => 'ken',
    mode        => '0400'
  }

  user { "mlee":
    ensure      => present,
    uid         => '502',
    shell       => '/bin/bash',
    home        => '/home/mlee', 
    managehome  => true,
    groups => ["wheel"],
  }

  group { "mlee":
    gid         => '502'
  }

  file { "/home/mlee/.ssh":
    ensure      => directory,
    owner       => 'mlee',
    group       => 'mlee',
    mode        => '0700',
  }
  
  file { "/home/mlee/.ssh/id_rsa.pub":
    source      => 'puppet:///modules/users/mlee/id_rsa.pub',
    ensure      => file,
    owner       => 'mlee',
    group       => 'mlee',
    mode        => '0400'
  }

  file { "/home/mlee/.ssh/authorized_keys":
    source      => 'puppet:///modules/users/mlee/authorized_keys',
    ensure      => file,
    owner       => 'mlee',
    group       => 'mlee',
    mode        => '0400'
  }

	user { "jli":
		ensure			=> present,
		uid					=> '503',
		shell				=> '/bin/bash',
		home				=> '/home/jli',
		managehome	=> true,
    groups => ["wheel"],
	}

	group { "jli":
		gid					=> '503',
	}

	file { "/home/jli/.ssh":
		ensure			=> directory,
		owner				=> 'jli',
		group				=> 'jli',
		mode				=> '0700',
	}

  file { "/home/jli/.ssh/id_rsa.pub":
    source      => 'puppet:///modules/users/jli/id_rsa.pub',
    ensure      => file,
    owner       => 'jli',
    group       => 'jli',
    mode        => '0400'
  }

  file { "/home/jli/.ssh/authorized_keys":
    source      => 'puppet:///modules/users/jli/authorized_keys',
    ensure      => file,
    owner       => 'jli',
    group       => 'jli',
    mode        => '0400'
  }


	user { "sraghuram":
		ensure			=> present,
		uid					=> '504',
		shell				=> '/bin/bash',
		home				=> '/home/sraghuram',
		managehome	=> true,
    groups => ["wheel"],
	}

	group { "sraghuram":
		gid					=> '504',
	}

	file { "/home/sraghuram/.ssh":
		ensure			=> directory,
		owner				=> 'sraghuram',
		group				=> 'sraghuram',
		mode				=> '0700',
	}

  file { "/home/sraghuram/.ssh/id_rsa.pub":
    source      => 'puppet:///modules/users/sraghuram/id_rsa.pub',
    ensure      => file,
    owner       => 'sraghuram',
    group       => 'sraghuram',
    mode        => '0400'
  }

  file { "/home/sraghuram/.ssh/authorized_keys":
    source      => 'puppet:///modules/users/sraghuram/authorized_keys',
    ensure      => file,
    owner       => 'sraghuram',
    group       => 'sraghuram',
    mode        => '0400'
  }

  
	user { "smingolelli":
		ensure			=> present,
		uid					=> '505',
		shell				=> '/bin/bash',
		home				=> '/home/smingolelli',
		managehome	=> true,
    groups => ["wheel"],
	}

	group { "smingolelli":
		gid					=> '505',
	}

	file { "/home/smingolelli/.ssh":
		ensure			=> directory,
		owner				=> 'smingolelli',
		group				=> 'smingolelli',
		mode				=> '0700',
	}

  file { "/home/smingolelli/.ssh/id_rsa.pub":
    source      => 'puppet:///modules/users/smingolelli/id_rsa.pub',
    ensure      => file,
    owner       => 'smingolelli',
    group       => 'smingolelli',
    mode        => '0400'
  }

  file { "/home/smingolelli/.ssh/authorized_keys":
    source      => 'puppet:///modules/users/smingolelli/authorized_keys',
    ensure      => file,
    owner       => 'smingolelli',
    group       => 'smingolelli',
    mode        => '0400'
  }

}
