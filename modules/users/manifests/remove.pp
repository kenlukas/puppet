class users::remove {


  file { "/home/ken/.ssh/authorized_keys":
    ensure => absent,
  }
  file { "/home/ken/.ssh/id_rsa.pub":
    ensure => absent,
  }
	file { "/home/ken/.ssh":
    ensure => absent,
  }
  # Not needed, it happens in user ensure absent
	#group { "ken":
  #  ensure => absent,
  #}
	user { "ken":
    ensure => absent,
    managehome => true,
  }
}
